/**
 * Created by LeutrimNeziri on 01/03/2019.
 */
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import withStyles from 'ui/withStyles'

const styles = ({size, transitions}) => ({
})

const Link = props => <a {...props}/>

export default withStyles(styles)(Link)
