/**
 * Created by LeutrimNeziri on 22/03/2019.
 */

const horizontalOptions = {
  left: 'left',
  middle: 'middle',
  right: 'right'
}

const verticalOptions = {
  top: 'top',
  center: 'center',
  bottom: 'bottom'
}
export const popoverPositions = {
  horizontalOptions,
  verticalOptions
}

export default {
  popoverPositions
}