/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
module.exports = {
  PAGES: {
    HOME: 'home',
    LECTURE_1: {
      ID: 'lecture1',
      GETTING_STARTED: 'gettingstarted',
      PROJECT_SETTUP: 'projectsetup',
      AGILE_METHODOLOGY: 'agile',
      WAY_OF_WORKING: 'wayofworking'
    },
    LECTURE_2: {
      ID: 'lecture2'
    },
    GLOSSARY: 'glossary',
    RESOURCES: 'resources',
    PLAN_PROGRAM: 'program'
  },
}


